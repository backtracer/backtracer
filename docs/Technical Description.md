# Technical description of Backtracer


## Requires
- [Node JS](https://nodejs.org/en/download/)
 
    - Once installed, you must install globally [Nest](https://nestjs.com/) and [Angular](https://angular.io/) `npm install -g @nestjs/cli @angular/cli`

- [Docker](https://docs.docker.com/get-docker/) 


### **The Postgres Database**
The Postgres Database runs using Docker
In the dockerized part of this project, you can find a persistent postgres database. A database is needed to easily query the GTFS files' content.
To import the GTFS file into the postgres database, the tool used was [GTFSDB](https://github.com/OpenTransitTools/gtfsdb).

The Postgres image used for Docker is the official one and an adminer is also available

### **The Front Composant**
The Front part of the application can be run like others node project. Make sure to have already execute a npm install inside the `Front-backtracer` folder


### **The API Composant**
The API part of the application can be run like others node project. Make sure to have already execute a npm install inside the `API-backtracer` folder

