# Contribute to bakctracer

## Who can contribute ?

**Everybody can contribute to this project !**

## How to contribute ? 

There are few ways to contribute to this project

### Report a bug
To report a bug, you can open an [issue of type accident](https://gitlab.com/backtracer/backtracer/-/issues/new?issue[title]=BUG%20&issue[issue_type]=incident&issue[description]=Explorer%20used:%20%0AProblem:%20). To contribute completly, you must tell us many informations like the explorer used, the version of the explorer, the use case (what did you do to produce/discover this bug). 

Once the bug reported, you can track the progression of the resolution with given link after validating the report.

### Fix a bug 

To contribute, you need to open an issue. Here, you can develop your meanings and the way to proceed. Once the fix explained, you can ask for the opinions of others contributors to complete or to perfect the future development. 

Now that your idea is more precise, you can start the deployement in a branch named  [***"BUGFIX-XX"***](https://gitlab.com/backtracer/backtracer/-/branches/new?branch_name=BUGFIX-XX) where XX matches the issue number. In the last commit of your development, don't forget to mention #XX in the commit message ! It will permit to activate the issue tracker of GitLab.

### Share an idea of feature

If you have an idea of feature for the program, you can share it by [opening an issue of type Issue](https://gitlab.com/backtracer/backtracer/-/issues/new?issue[title]=EVO%20&issue[issue_type]=issue).
Developer or not developer, you can give your opinion by reacting 👍 or 👎 if you this that this is a good idea or not



### Develop a feature

In the ticket created, you can develop your meanings and the way to proceed. Once the idea developed, you can ask for the opinions of others contributors to complete or to perfect the future development. 

Now that your idea is more precise, you can start the deployement in a branch named [***"DEVELOP-XX"***](https://gitlab.com/backtracer/backtracer/-/branches/new?branch_name=DEVELOP-XX) where XX matches the issue number. In the last commit of your development, don't forget to mention #XX in the commit message ! It will permit to activate the issue tracker of GitLab.

### Project management

If you need some information or if you want to contribute to this project by another way, you can open a [ticket of type PROJECT](https://gitlab.com/backtracer/backtracer/-/issues/new?issue[title]=PROJECT%20&issue[issue_type]=issue)










When your development is finished, you can create a Merge Request where you have to mentions at least 3 contributors (those following the issue in priority)